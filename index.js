const express = require('express')
var cors = require('cors')
var fs = require('fs')
const bodyParser = require('body-parser');
const app = express()
var router = express.Router()
const port = 3000
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
const jwt = require("jsonwebtoken");
const expressJwt = require('express-jwt')
const dotenv = require("dotenv");
// get config vars
dotenv.config();
var secretTokenPriv = fs.readFileSync('server.key');  // get private key
var secretTokenPub = fs.readFileSync('server.crt');
var jwt_decode  = require("jwt-decode");

router.use(function (req, res, next) {
  if(req.path !== '/api/create') {
      var token = null;
      if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
          token = req.headers.authorization.split(' ')[1];
           var decoded = jwt_decode(token);
           if(decoded && decoded.iss === 'B2C') {
              verifyToken(req, res, next);
           } else {
              next()
           }
      } else {
          next()
      }
  }
})

router.get('/', (req, res) => {
	res.send('Hello World!')
});
router.post('/api/create', (req, res) => {
  const token = generateAccessToken({ username: req.body.username, alg: 'RS256', iss: 'B2C' });
  res.json({token});
});
router.get('/api/users', /*authenticateToken,*/ (req, res) => {
	res.send('get list of users on secured route');
});
function generateAccessToken(data) {
  return jwt.sign(data, secretTokenPriv, { algorithm: 'RS256', expiresIn: '24h' });
}
function verifyToken(req, res, next) {
  // Gather the jwt access token from the request header
  var token = null;
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
      token = req.headers.authorization.split(' ')[1]
  }
  if (token == null) return res.sendStatus(401) // if there isn't any token

  jwt.verify(token, secretTokenPub, { algorithms: ['RS256'] }, (err, decoded) => {
    if (err) return res.sendStatus(403)
    req.user = decoded
    next() // pass the execution off to whatever request the client intended
  })
}

app.use('/', router);
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})